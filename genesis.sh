#!/bin/bash

###################################################
# First Bash Shell script to execute psql command
###################################################

#Set the value of variable
#host="localhost"
#port="5432"
#dbSource="genesis"
#dbDestination="genesis11"
#user="postgres"
#sourceSchema="schema_backup.sql"
#sourceData="data_backup.bkp"

#** Drop schema
psql -h $host -p $port -d $dbDestination -U $user -c "DROP SCHEMA IF EXISTS dataview, site, portal, onms  CASCADE;"
#** dump schema
pg_dump -h $host -p $port -d $dbSource -U $user -v -s -n dataview -n site -n portal -n onms > $sourceSchema

#** restore schema (using psql because plan data)
psql -h $host -p $port -d $dbDestination -U $user -v -s -n dataview -n site -n portal -n onms < $sourceSchema

#** dump data
pg_dump -h $host -p $port -d $dbSource -U $user -v -a -n dataview -n portal -n site -n onms -Fc -f $sourceData
#** restore data  (using pg_restore because compress or custom data)
pg_restore -h $host -p $port -d $dbDestination -U $user -v -a -n dataview -n portal -n site -n onms -Fc $sourceData