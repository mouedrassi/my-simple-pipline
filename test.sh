#!/bin/bash

###################################################
# First Bash Shell script to execute psql command
###################################################

#Set the value of variable
host="172.20.20.3"
#host="newtown-db-1.prod.ntl.dv"
port="5432"
dbSource="dev"
#dbDestination="genesis11"
user="staging"
sourceSchema="dump/schema_backup.sql"
sourceData="dump/data_backup.bkp"

#** Drop schema
#psql -h $host -p $port -d $dbDestination -U $user -c "DROP SCHEMA IF EXISTS dataview, site, portal, onms  CASCADE;"
#** dump schema
pg_dump -h $host -p $port -d $dbSource -U $user -v -s -n dataview -n site -n portal -n onms > $sourceSchema

##** restore schema (using psql because plan data)
#psql -h $host -p $port -d $dbDestination -U $user -v -s -n dataview -n site -n portal -n onms < $sourceSchema
#
##** dump data
#pg_dump -h $host -p $port -d $dbSource -U $user -v -a -n dataview -n portal -n site -n onms -Fc -f $sourceData
##** restore data  (using pg_restore because compress or custom data)
#pg_restore -h $host -p $port -d $dbDestination -U $user -v -a -n dataview -n portal -n site -n onms -Fc $sourceData

